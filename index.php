<?php
require 'vendor/autoload.php';

use Illuminate\Support;
use App\Utils\Routes\Request;
use App\Utils\Routes\Router;
use App\Controllers\ExportController;
use App\Controllers\ReportController;
use App\Controllers\RosterController;
use App\Controllers\TeamController;

$router = new Router(new Request);

$router->get('/', function(){
    $cont = new ReportController;
    $data = $cont->getInitData();
    list(
        $_static_images,
        $top_player,
        $team_code,
        $teams,
        $teams_options,
        $best_3pt_team,
        $best_3pt_team_players,
        $best_3pt_player, 
    ) = $data;
    return include('views/home.php');
});

$router->get('/players', function($request) {
    $cont = new RosterController;
    $data = $cont->getPlayersDirectory(collect($request->getParams()));
    list(
        $letters_options, // Index Directory
        $alpha_list,  //list with Players Name
        $player_filtered, //Boolean
        $filter_by_letter,
        $table_header, // Table will diplay if it has filter parameters
        $table_row,    // Table will diplay if it has filter parameters
        $filtered_wrapper, // Selected Filter Paramaters ex. /players?post=SG&nationality=US
        $setDisable, // Disabled button export
        $tfp_label
    ) = $data;
    return include('views/players/all.php');
});

$router->get('/players/stats', function($request) {
    $cont = new RosterController;
    $request = collect($request->getParams());
    // Data Source;
    $data = $cont->getPlayerStats(collect(['id'=>$request->get('id'),'ws'=>1]));
    list($player, $stats) = $data;
    return include('views/players/player-stats.php');
});

$router->get('/teams', function($request) {
    $cont = new TeamController;
    // Data Source
    $team = $cont->getAllTeam();
    return include('views/teams/all.php');
});

$router->get('/teams/preview', function($request) {
    $cont = new TeamController;
    // Data Source
    $data = $cont->getTeamPreview(collect($request->getParams()));
    list(
        $team_code,
        $team,
        $players,
        $table_header, // Table for players
        $table_row,    // 
        $filtered_wrapper, // Selected Filter Paramaters ex. /teams?pos=SG&nationality=US
        $tfp_label
    ) = $data;
    return include('views/teams/preview.php');
});

$router->get('/reports', function($request){
    $controller = new ReportController();
    return $controller->getReport(collect($request->getParams()));
});
$router->get('/exports', function($request) {
    $controller = new ExportController();
    echo $controller->export(collect($request->getParams()));
});
$router->get('/getPlayersByTeamCode', function($request){
    $controller = new RosterController();
    echo $controller->filterPlayers(collect($request->getParams()));
});

$router->get('/page-not-found', function(){
    return include('views/error/page404.php');
});
