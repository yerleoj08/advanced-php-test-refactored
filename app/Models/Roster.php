<?php 
namespace App\Models;
use Illuminate\Support;
use App\Utils\Model;

class Roster extends Model {

    /*  You can easily change the connection pointing to another server by changing the server
        and Add your sever credentials to config/database
    */
    protected $table = 'roster';
    protected $connection = 'db_server'; 
    
    public function __construct(){
        parent::__construct( $this->connection ?? '' );
    }

    public function searchPlayer($request){

        // $where = []; 
        // if ($request->has('playerId')) $where[] = "r.id = '" . $request['playerId'] . "'";
        // /* Player statement is redandunt with Player ID */
        // // if ($request->has('player')) $where[] = "r.name = '" . $request['player'] . "'";
        // if ($request->has('team_code')) $where[] = "r.team_code = '" . $request['team_code']. "'";
        // // if ($request->has('position')) $where[] = "r.position = '" . $request['position'] . "'";
        // if ($request->has('pos')) $where[] = "r.pos = '" . $request['pos'] . "'";
        // // if ($request->has('country')) $where[] = "r.nationality = '" . $request['country'] . "'";
        // if ($request->has('nationality')) $where[] = "r.nationality = '" . $request['nationality'] . "'";
        // if(count($where)){
        //     $where = implode(' AND ', $where);
        // }

        /*  Other way option of statement,
            This design of code it's requires the paramater and field names from the model are both equal
        */
        
        $where = [];
        $req = collect($request); 

        $f = $req->get('f') ?? 0;
        $ws = $req->has('ws') ?? 0;
        $rpt = $req->has('rpt') ?? 0;

        /*
        * ws = with stats
        * fmt = format
        * rpt = report
        * f = filter by letter
        */
        $exclude_params = ['ws','fmt','rpt','filename','f'];
        
        if (is_object($req)){
            // Exclude the parameters that is not decluded in our model or table,
            if (is_array($exclude_params)){
                foreach($exclude_params as $ex){
                    unset($req[$ex]);
                }
            }
            // Format as statement query
            foreach($req as $key=>$value){
                $where[] = "r.$key = '$value'";
            }
            // We have our where statement now
            $where = "WHERE " . collect($where)->join(' AND ');
        }

        if($where=== "WHERE ") :
            $where = null;
        endif;

        switch( $ws ){
            case 0:
                $sql = "SELECT r.id, r.name, r.number, r.pos, r.height, r.weight, DATE_FORMAT(r.dob, '%b %d, %Y') as date_of_birth, 
                        r.nationality, r.years_exp, r.college, LEFT(SUBSTRING_INDEX(r.name, ' ', -1), 1) as first_letter_of_lastname
                    FROM $this->table AS r
                    $where";
                break;
            case 1:
                $sql = "SELECT r.id, r.name, r.number, r.pos, pt.age, r.height, r.weight, DATE_FORMAT(r.dob, '%b %d, %Y') as date_of_birth, 
                        r.nationality, r.years_exp, r.college, r.team_code, t.name as team, pt.games, pt.games_started, pt.minutes_played, 
                        pt.field_goals, pt.field_goals_attempted,pt.3pt, pt.3pt_attempted, pt.2pt, pt.2pt_attempted, pt.free_throws, 
                        pt.free_throws_attempted, pt.offensive_rebounds,pt.defensive_rebounds, pt.assists, pt.steals, pt.blocks, pt.turnovers, pt.personal_fouls,
                        ((pt.3pt * 3) + (pt.2pt * 2) + (pt.free_throws)) as total_points,
                        ROUND(((pt.3pt / pt.3pt_attempted) * 100), 2) as 3pt_pct,
                        ROUND(((pt.2pt / pt.2pt_attempted) * 100), 2) as 2pt_pct,
                        ROUND(((pt.free_throws / pt.free_throws_attempted) * 100), 2) as free_throws_pct,
                        (pt.offensive_rebounds + pt.defensive_rebounds) as total_rebounds,
                        ROUND(((pt.3pt / pt.3pt_attempted / pt.minutes_played) * 100), 2) as overall_3pt_pct,
                        LEFT(SUBSTRING_INDEX(r.name, ' ', -1), 1) as first_letter_of_lastname
                    FROM player_totals AS pt
                    INNER JOIN $this->table AS r ON r.id = pt.player_id
                    LEFT JOIN team AS t
                        ON t.code = r.team_code
                    $where
                    ORDER BY CAST(3pt_pct as FLOAT) DESC";
                break;
        }

        $data = $this->executeQuery($sql) ? : []; 
        
        // If we are getting the report and we need to filter the data with the first letter of the players last name and
        // since we cannot include that in our statement we better filter the data like

        foreach ($data as $index=>&$row) { 
            if($rpt){
                if($f){
                    if( strtolower($row['first_letter_of_lastname']) !== strtolower($f)){
                        unset($data[$index]);
                    }
                }
                unset($row['id']); 
            }
            unset($row['first_letter_of_lastname']); 
        }
       
        return collect($data);
        
        // we dont need this code below because it is included in our query

        // if($ws){
        //     foreach ($data as &$row) {
        //         $row['total_points'] = ($row['3pt'] * 3) + ($row['2pt'] * 2) + $row['free_throws'];
        //         $row['field_goals_pct'] = $row['field_goals_attempted'] ? floor(round($row['field_goals'] / $row['field_goals_attempted'], 2) * 100)  : 0;
        //         $row['3pt_pct'] = $row['3pt_attempted'] ? floor(round($row['3pt'] / $row['3pt_attempted'], 2) * 100) : 0;
        //         $row['2pt_pct'] = $row['2pt_attempted'] ? floor(round($row['2pt'] / $row['2pt_attempted'], 2) * 100) : 0;
        //         $row['free_throws_pct'] = $row['free_throws_attempted'] ? floor(round($row['free_throws'] / $row['free_throws_attempted'], 2) * 100)  : 0;
        //         $row['total_rebounds'] = $row['offensive_rebounds'] + $row['defensive_rebounds'];
        //     }
        //     return collect($data);
        // } 
        // return collect($data);
    }

}
