<?php 
namespace App\Models;
use Illuminate\Support;
use App\Utils\Model;

class PlayerOfTotal extends Model {

    /*  You can easily change the connection pointing to another server by changing the server
        and Add your sever credentials to config/database
    */

    protected $table = 'player_totals';
    protected $connection = 'db_server'; 
    
    public function __construct(){
        parent::__construct( $this->connection ?? '' );
    }
}
