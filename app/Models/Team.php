<?php 
namespace App\Models;
use Illuminate\Support;
use App\Utils\Model;

class Team extends Model {

    /*  You can easily change the connection pointing to another server by changing the server
        and Add your sever credentials to config/database
    */

    protected $table = 'team';
    protected $connection = 'db_server'; 
    
    public function __construct(){
        parent::__construct( $this->connection ?? '' );
    }

    public function getByCode($code){
        $sql = "SELECT * FROM $this->table WHERE code = '$code'";
        $data = $this->executeQuery($sql);
        return $data;
    }

}
