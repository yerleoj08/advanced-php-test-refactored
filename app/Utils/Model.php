<?php
namespace App\Utils;
use Illuminate\Support;
use App\Controllers\Controller;
use App\Utils\Connection;

class Model extends Controller
{
    public $mySql;

    public function __construct($db_server){
        try{
            $new_connection = new Connection($db_server);
            $this->mySql = $new_connection->connect();
        }catch(\Throwable $th){
            $this->fail($th);
        }
    }

    public function all(){ 
        $sql = "SELECT * FROM $this->table";
        $data = $this->executeQuery($sql);
        return $data;
    }

    public function getById($id){
        $sql = "SELECT * FROM  $this->table WHERE id = $id";
        $data = $this->executeQuery($sql);
        return $data;
    }

    public function query($sql){
        $data = $this->executeQuery($sql);
        return $data;
    }

    public function executeQuery($sql){
        $data = [];
        try{
            if(empty(trim($sql))){
                return $this->fail('Invalid query');
            }
            $result = $this->mySql->query($sql);
            // return  collect($result);
            if($result){
                while($row = mysqli_fetch_assoc($result)){
                    $data[]= $row;
                }
                return $data;
            }
            return [];
        }catch(\Throwable $th){
            return $this->fail("An error execute query $th");
        }
    }
}
