<?php
namespace App\Utils;

use Illuminate\Support;
use App\Controllers\Controller;

class Connection extends Controller 
{
    protected $config;
    protected $connect_to_server; 

    function __construct($server){
        $this->config = include($_SERVER['DOCUMENT_ROOT'].'/config/database.php');
        $this->connect_to_server = trim($server) ==='' || trim($server) === null ? $this->config['default'] : $server;
    }
    function connect(){
        try {
            $config = $this->config[$this->connect_to_server];
            $mysqli = new \mysqli($config['DB_HOST'], $config['DB_USER'], $config['DB_PASS'], $config['DB_NAME']);
            if ($mysqli->connect_errno) {
                $this->fail("Connection failed: $mysqli->connect_error");
                // return header('Location: /views/page404.php');
            }
        } catch (mysqli_sql_exception $e) {
            $this->fail("Connection error $e");
        }
        return $mysqli;
    }

}