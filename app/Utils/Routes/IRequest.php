<?php
namespace App\Utils\Routes;
interface IRequest {
    public function getBody();
    public function getParams();
}