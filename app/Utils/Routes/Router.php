<?php
namespace App\Utils\Routes;
use App\Utils\Routes\IRequest;

class Router {

    private $request;
    private $supportedHttpMethods = ["GET","POST"];

    function __construct(IRequest $request){
        $this->request = $request;
    }

    function __call($name, $args){

        list($route, $method) = $args;
        if(!in_array(strtoupper($name), $this->supportedHttpMethods)){
            $this->invalidMethodHandler();
        }

        $this->{strtolower($name)}[$this->formatRoute($route)] = $method;
    }

    /**
     * Removes trailing forward slashes from the right of the route.
     * @param route (string)
     */
    private function formatRoute($route){
        $result = rtrim($route, '/');
        if ($result === '') {
            return '/';
        }
        return $result;
    }

    private function invalidMethodHandler(){
        header("{$this->request->serverProtocol} 405 Method Not Allowed");
    }

    private function defaultRequestHandler(){
        header("{$this->request->serverProtocol} 404 Not Found");
        header("Location: /page-not-found");
    }

    /**
     * Resolves a route
     */
    function resolve(){
        $methodDictionary = $this->{strtolower($this->request->requestMethod)};
        $formatedRoute = $this->formatRoute($this->request->requestUri);
        $method = $methodDictionary[explode("?",$formatedRoute)[0]] ?? null;
        if(is_null($method)){
            $this->defaultRequestHandler();
            return;
        }
        return call_user_func_array($method, array($this->request));
    }

    function __destruct(){
        $this->resolve();
    }
}