<?php
namespace App\Utils;

use Illuminate\Support;
use LSS\Array2Xml;
use App\Controllers\Controller;

class Exporter extends controller {
    
    public $filename;
    public $title;

    public function __construct($filename, $title) {
        $this->filename = $filename ?? date('y_m_d_h_i');
        $this->title = $title;
    }
    public function format($data, $format = 'html') {
        switch($format) {
            case 'xml': 
                $export = $this->xmlFormat($data);
                break;
            case 'json':
                $export = $this->jsonFormat($data);
                break;
            case 'csv':
                $export = $this->csvFormat($data);
                break;
            default:
                $export = $this->htmlFormat($data);
                break;
        }
        if ($export){
            header("Content-type: text/{$format}");
            header("Content-Disposition: attachment; filename={$this->filename}.{$format};");
            return $export;
        }
    }
    public function csvFormat($data){
        $csv = [];
        $headings = $this->getHeader($data);
        $csv[] = $headings->join(',');
        foreach ($data as $dataRow) {
            $csv[] = implode(',', array_values($dataRow));
        }
        return implode("\n", $csv);
    }
    public function jsonFormat($data){
        return json_encode($data->all());
    }
    public function xmlFormat($data){
        header('Content-type: text/xml');
        // fix any keys starting with numbers
        $keyMap = ['zero', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine'];
        $xmlData = [];
        // output data
        foreach ($data->all() as $row) {
            $xmlRow = [];
            foreach ($row as $key => $value) {
                $key = preg_replace_callback('(\d)', function($matches) use ($keyMap) {
                    return $keyMap[$matches[0]] . '_';
                }, $key);
                if( $value !=='')
                $xmlRow[$key] = $value;
            }
            $xmlData[] = $xmlRow;
        }
        $xml = Array2XML::createXML('data', ['entry'=>$xmlData]);
        return $xml->saveXML();
    }
    public function htmlFormat($data){
        $headings = $this->getHeader($data);
        if (!is_object($headings)){
            return false;
        }
        $headings = "<tr><th>". $headings->join('</th><th>') ."</th></tr>";
        // output data
        $rows = [];
        foreach ($data as $dataRow) {
            $row = '<tr>';
                foreach ($dataRow as $key => $value) {
                    $row .= "<td>$value</td>";
                }
                $row .= '</tr>';
            $rows[] = $row;
        }
        $rows = implode('', $rows);
        return $this->htmlTemplate("<table> {$headings} {$rows} </table>");
    }
    // wrap html in a standard template
    public function htmlTemplate($html) {
        return <<<HTML
        <html>
            <head>
            <style type="text/css">
                body {
                    font: 12px Roboto, Arial, Helvetica, Sans-serif;
                }
                table{
                    margin:auto;
                }
                h1{
                    text-align:center;
                }
                td, th {
                    padding: 4px 8px;
                    font-size: 12px;
                    white-space: nowrap;
                }
                th {
                    background: rgb(5, 7, 31);
                    font-weight: 700;
                    color: #fff;
                }
                tr:nth-child(odd) {
                    background: #f4f4f4;
                }
                .containera{
                    padding: 0;
                    margin-top: 1em;
                    border:1px solid green;
                }
            </style>
            </head>
            <body>
                <div class="container">
                    <h1>NBA 2019 - {$this->title}</h1>
                    $html
                </div>
            </body>
        </html>
        HTML;
    }
}

?>