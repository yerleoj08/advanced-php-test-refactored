<?php
namespace App\Utils;
use Illuminate\Support;
use App\Controllers\Controller;
use App\Utils\Connection;

class DB extends Controller
{
    public $default;
    public $mySql;
    public $conn;

    public function __construct(){
        try{
            $config = include($_SERVER['DOCUMENT_ROOT'].'/config/database.php');
            $conn = new Connection($config['default']);
            $this->conn = $conn->connect();
            $this->default = $config['default'];
        }catch(\Throwable $th){
            self::fail($th);
        }
    }
    
    public function newConnection($server){
        $conn = new Connection($server);
        return $conn->connect();
    }

    public function query($sql){
        $conn =  self::newConnection('db_server');
        return $conn->query($sql);
    }

    public static function connection($db_server){
        return self::newConnection($db_server);
    }

}
