<?php
namespace App\Controllers;
use Illuminate\Support;
use App\Controllers\Controller;
use App\Models\Team;
use App\Models\Roster;
use App\Utils\DB;

class ReportController extends Controller {

    public function __construct() {
    }
    
    public function getAllTeam() {
        $team = new Team();
        $data = $team->all();
        return collect($data);
    }

    public function getTopPlayers() {
        $roster = new Roster();
        $data = $roster->all();
        return collect($data)->take(12);
    }

    public function getPlayerByTeamCode($code){
        $roster = new Roster();
        $data = $roster->searchPlayer($code);
        return collect($data)->take(6);
    }

    public function getReport($request){
        $report_type = $request->get('type') ? : 1;
        $best_of = $request->get('best_of') ? : 1;
        // $best_of = 2;
        $sql = '';
        switch ((int)$report_type) {
            case 1:
                //Best 3 Pointer Player
                $sql = "SELECT r.id, r.name, r.number, r.pos, pt.age, r.height, r.weight,  DATE_FORMAT(r.dob, '%b %d, %Y') as date_of_birth, r.nationality, r.years_exp, r.college, 
                        r.team_code, t.name as team, pt.games, pt.games_started, pt.minutes_played, pt.field_goals, pt.field_goals_attempted,
                        pt.3pt, pt.3pt_attempted, pt.2pt, pt.2pt_attempted, pt.free_throws, pt.free_throws_attempted, pt.offensive_rebounds,
                        pt.defensive_rebounds, pt.assists, pt.steals, pt.blocks, pt.turnovers, pt.personal_fouls,
                        ((pt.3pt * 3) + (pt.2pt * 2) + (pt.free_throws)) as total_points,
                        ROUND(((pt.3pt / pt.3pt_attempted) * 100), 2) as 3pt_pct,
                        ROUND(((pt.2pt / pt.2pt_attempted) * 100), 2) as 2pt_pct,
                        ROUND(((pt.free_throws / pt.free_throws_attempted) * 100), 2) as free_throws_pct,
                        (pt.offensive_rebounds + pt.defensive_rebounds) as total_rebounds,
                        ROUND(((pt.3pt / pt.3pt_attempted / pt.minutes_played) * 100), 2) as overall_3pt_pct
                    FROM player_totals AS pt
                    LEFT JOIN roster AS r
                        ON r.id = pt.player_id
                    LEFT JOIN team AS t
                        ON t.code = r.team_code
                    ORDER BY CAST(overall_3pt_pct as FLOAT)  DESC LIMIT $best_of";
                    break;
            case 2:
                 //Best 3 Pointer Team
                $sql = "SELECT t.code, t.name as team, ROUND((SUM(pt.3pt) / SUM(pt.3pt_attempted) * 100), 2) as overal_3pt_team_pct
                    FROM player_totals as pt
                    LEFT JOIN roster as r
                        ON r.id = pt.player_id
                    LEFT JOIN team as t
                        ON t.code = r.team_code
                    GROUP BY team 
                    ORDER BY overal_3pt_team_pct DESC LIMIT $best_of";
                break;
            default:  
        }

        if($sql){
            // $data = DB::connection('db_server_2')->query($sql); // defined connection
            $data = DB::query($sql); // default connection
            return collect($data);
        }
        $this->fail('Opss! Something went wrong.');
    } 

    public function getInitData(){

        $basUri = 'https://www.basketball-reference.com/req/202011052/images/players';
    
        $_static_images = [
            "$basUri/johnske02.jpg",
            "$basUri/klebima01.jpg",
            "$basUri/martico01.jpg",
            "$basUri/smithis01.jpg",
            "$basUri/nichoan01.jpg",
            "$basUri/spellom01.jpg",
            "$basUri/richaja01.jpg",
            "$basUri/adrieje01.jpg",
            "$basUri/cartevi01.jpg",
            "$basUri/kerrst01.jpg",
            "$basUri/amundlo01.jpg",
            "$basUri/perkisa01.jpg"
        ];
    
        $best_3pt_team_players = []; $teams = ''; $teams_options = "<option value='' >- Select Team -</option>"; $total_players = 0; 
       
        // Data Source
        $featured_players = $this->getTopPlayers();
        $allTeam = $this->getAllTeam();

        // Best 3 pointers

        // player
        $best_3pt_player = $this->getReport(collect(['type'=>1,'best_of'=>1]));
        // dd($best_3pt_player);

        // Team
        $best_3pt_team = $this->getReport(collect(['type'=>2]));
        if(count($best_3pt_team)){
            $best_3pt_team_players = $this->getPlayerByTeamCode(collect(['team_code'=>$best_3pt_team[0]['code'],'ws'=>1]));
            // dd($best_3pt_team_players);
        }
        // Best 3 Point Team Logo display
        $team_code = !count($best_3pt_team) ? '' : $best_3pt_team[0]['code'];
        $best_3pt_team = "<a href='/teams/preview?team_code=$team_code'><img src='/public/assets/images/logo/$team_code.png' width='100%' alt='logo'></a>";
        

        // Top Player Initial Display
        $top_player = !count($featured_players) ? 'No data has found' : '';
        foreach($featured_players as $key=>$player){
            $top_player .= "<div class='col-4 col-sm-3 col-lg-2 p-0'><div class='image-wrapper'><a href='/players/stats?id={$player['id']}'><img src='$_static_images[$key]' width='100%' alt='image'></a></div></div>";
        }

        foreach($allTeam as $child){
            $fake_win = rand(10,100); $fake_loss= rand(0,10);
            $teams .= "<tr><td><a href='/teams/preview?team_code={$child['code']}' >{$child['name']}</a></td><td>$fake_win</td><td>$fake_loss</td></tr>";
            $teams_options .= "<option value='{$child['code']}' >{$child['name']}</option>";
        }

        return [
            $_static_images,
            $top_player,
            $team_code,
            $teams,
            $teams_options,
            $best_3pt_team,
            $best_3pt_team_players,
            $best_3pt_player, 
        ];
    }
}