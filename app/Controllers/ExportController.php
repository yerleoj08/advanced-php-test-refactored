<?php
namespace App\Controllers;
use Illuminate\Support;
use App\Controllers\Controller;
use App\Utils\Exporter;
use App\Models\Roster;

class ExportController extends Controller {

    public function __construct() {
        
    }
    public function export($request) {
        
        $roster = new Roster();
        $data = $roster->searchPlayer($request);
        if (!$data) {
            return $this->fail("Error: No data found!");
        }
        
        $format = $request->get('fmt') ?? 'html';
        $title = $request->get('filename'); 
        $filename = $title .'_'. time();
        
        $exporter = new Exporter($filename, $title);
        return $exporter->format($data, $format);
    }
}