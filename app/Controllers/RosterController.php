<?php
namespace App\Controllers;
use App\Controllers\Controller;
use App\Models\Roster;

class RosterController extends Controller {

    public function __construct() {

    }

    public function getAllPlayers() {
        $roster = new Roster();
        return $roster->all();
    }

    public function filterPlayers($request) {
        $roster = new Roster();
        return $roster->searchPlayer($request);
    }

    public function getPlayerStats($request){
        $stats = '';
        $data = $this->filterPlayers(collect(['id'=>$request->get('id'),'ws'=>1]));

        // Get all columns from roster to unset from player info fields from stats we dont need personal info to our stats details;
        $roster_columns = collect(collect($this->getAllPlayers())->get(0))->keys(); 
        $exclude_this_fields = array_merge($roster_columns->toArray() , array("team","player_id","age","date_of_birth"));
        
        // Get Stats
        foreach($data as $row){
            foreach ($row as $key => $value) {
                if( !in_array($key, $exclude_this_fields) ){
                    $value = $key ==='minutes_played' ? $this->minutesToHours($value) : (strlen(explode(".",$value)[0]) >2 ? number_format($value) : $value) ;
                    $stats .= '<div class="col-md-4"><div class="row"><div class="col-7 label">'. $this->properCase(str_replace('_',' ',$this->replaceAbbre($key))) .'</div><div class="col-4  value ">'. $value .'</div></div></div>';
                    // $stats[] = [$this->properCase(str_replace('_',' ',$this->replaceAbbre($key))) => ( strlen(explode(".",$value)[0]) >2 ? number_format($value) : $value) ];
                }
            }
        }
        return [(collect($data)->first() ?? []), "<div class='row'>$stats</div>"];
    }

    public function getPlayersDirectory($request){

        $letters = range('a','z'); $filtered_wrapper=''; $player_filtered = false;  $filter_by_letter = ''; $table_row = ''; $tfp = 0;
    
        $exclude_fields = ['id','team_code'];
        $filter_by_letter = $this->upperCase($request->get('f')); 
        // Data Source
        // unset($request['f']);
        $players = $this->filterPlayers($request);
    
        // Table Header Init
        $table_header = $this->getHeader($players, $exclude_fields); 
        if($table_header){
            $table_header = '<tr><th>' . $table_header->join('</th><th>') . '</th></tr>';
        }   
    
        // Data Table Row Filtered
        foreach ($players as $row) {
            // Row Values
            $dataRow = '<tr>';
                foreach ($row as $key => $value) {
                    if( !in_array($key, $exclude_fields) ){
                        if($key ==='nationality'){
                            $value = "<a href='javascript:void(0)' data-key='$key' data-value='$value' class='add_to_filter' >{$this->upperCase($value)}</a>";
                        }
                        if($key ==='pos'){
                            $value = "<a href='javascript:void(0)' data-key='$key' data-value='$value' class='add_to_filter' >$value</a>";
                        }
                        if($key==='name'){
                            $dataRow .= '<td><a href="/players/stats?id='.$row['id'].'">' . (!empty($value) ? $value : 'N/A') . '</a></td>';
                        } else {
                            $dataRow .= '<td>'. (!empty($value) ? $value : 'N/A') .'</td>';
                        }
                    }
                }
            $dataRow .= '</tr>';
            
            // Single Filter by first letter of the last name Letter
            // Get first letter of the last name;
            $name = explode(' ',$row['name']);
            $index = count($name) -1;
            $last_name = $name[$index][0]; 
    
            if($filter_by_letter){
                $player_filtered = true;
                if( $this->lowerCase($filter_by_letter) === $this->lowerCase($last_name) ){
                    $tfp +=1;
                    $table_row .= $dataRow;
                }
            }else{
                // filter with other paramters
                if(count($request)){
                    $player_filtered = true;
                    $tfp +=1;
                    $table_row .= $dataRow;
                }
            }
        }

        // Display filtered Parameter as button
        if(count($request)){
            foreach($request as $key=>$value){
                $filtered_wrapper .= "<a href='javascript:void(0)' data-key='$key' data-value='$value' class='remove_from_filter rounded text-white active px-2 py-1 bg-info fs--1 mr-1'>{$this->upperCase($value)}</a>";
            }
            $filtered_wrapper = "<div class='p-1 ml-auto ' >$filtered_wrapper</div>";
        }
    
        // Total Players found
        if($tfp===0){
            $setDisable = 'disabled';
            $table_header = '';
            $tfp_label = "No data has found";
        } else {
            $setDisable = '';
            $tfp_label = $tfp >1 ? "$tfp Players" : "$tfp Player";
        }
        
        // If there is no filtering paramters the initial display will be the Player Directory
    
        $letters_options = ''; $alpha_list = ''; $alpha_base = ''; $player_name_link =''; $base_letter ='';

        foreach($letters as $letter){ 
            $set_active_letter = $this->lowerCase($letter) === $this->lowerCase($filter_by_letter) ? 'text-dark-red' : '';
            $letters_options .= "<a href='/players?f=$letter' class='fs-3 ml-2 $set_active_letter' >{$this->upperCase($letter)}</a>";
            $base_letter = "<a href='/players?f=$letter' class='base_letter' >{$this->upperCase($letter)}</a>";
            foreach($players as $pl){
                $name = explode(' ',$pl['name']);
                $index = count($name) -1;
                $last_name = $name[$index];
                if( $this->lowerCase($letter) === $this->lowerCase($last_name[0])){
                    $player_name_link .= "<a href='/players/stats?id={$pl["id"]}' class='' >{$pl["name"]}</a>";
                }
            }
            
            if(!count($players)):
                $alpha_list = '<h3 class="text-dark-red my-2">No data has found</h3>';
            else :
                $alpha_list .= "<div class='d-flex'><div class='' >$base_letter</div><div class='name_list'>$player_name_link</div></div>";
            endif;

            $base_letter = ''; $player_name_link ='';
        }


        return array(
            $letters_options, // Index Directory
            $alpha_list,  //list with Players Name
            $player_filtered, //Boolean
            $filter_by_letter, // Filter by letter Directory
            $table_header, // Table will diplay if it has filter parameters
            $table_row,    // Table will diplay if it has filter parameters
            $filtered_wrapper, // Selected Filter Paramaters ex. /players?pos=SG&nationality=US
            $setDisable, // Disabled button export
            $tfp_label
        );
    }
}