<?php
namespace App\Controllers;
use Illuminate\Support;
use App\Controllers\Controller;
use App\Models\Team;
use App\Models\Roster;

class TeamController extends Controller {

    public function __construct() {

    }
    public function getAllTeam() {
        $team = new Team();
        $data = $team->all();
        return $data;
    }
    public function getTeamByCode($code) {
        $team = new Team();
        $data = $team->getByCode($code);
        return $data;
    }

    public function getPlayersByTeam($request) {
        $roster = new Roster();
        $data = $roster->searchPlayer($request);
        return collect($data);
    }

    public function getTeamPreview($request){
        $team = []; $players = []; $table_row = ''; $filtered_wrapper = '';
        $cont = new TeamController();

        $team_code = $request->get('team_code');
        if ($team_code){
            // Data Source Team
            $team = $cont->getTeamByCode($team_code);

            // Data Source Players
            $players = $cont->getPlayersByTeam(collect($request));

            $exclude_fields = ['id'];
            $table_header = $cont->getHeader($players, $exclude_fields); 
            if(is_object($table_header)){
                $table_header = '<tr><th>' . $table_header->join('</th><th>') . '</th></tr>';
            }
            foreach ($players as $row) {
                $dataRow = '<tr>';
                foreach ($row as $key => $value) {
                    if( !in_array($key, $exclude_fields) ){
                        $value = !empty($value) ? $value : 'N/A';
                        if($key ==='nationality'){
                            $value = "<a href='javascript:void(0)' data-key='$key' data-value='$value' class='add_to_filter' >{$cont->upperCase($value)}</a>";
                        }
                        if($key ==='pos'){
                            $value = "<a href='javascript:void(0)' data-key='$key' data-value='$value' class='add_to_filter' >$value</a>";
                        }
                        if($key==='name'){
                            $dataRow .= '<td><a href="/players/stats?id='.$row['id'].'">' . (!empty($value) ? $value : 'N/A') . '</a></td>';
                        } else {
                            $dataRow .= '<td>'. (!empty($value) ? $value : 'N/A') .'</td>';
                        }
                    }
                }
                $dataRow .= '</tr>';
                $table_row.=$dataRow;
            }
        } 
        // Display all filtered 
        if(count($request)){
            unset($request['team_code']);
            foreach($request as $key=>$value){
                $filtered_wrapper .= "<a href='javascript:void(0)' data-key='$key' data-value='$value' class='remove_from_filter rounded text-white active px-2 py-1 bg-info fs--1 mr-1'>{$cont->upperCase($value)}</a>";
            }
            $filtered_wrapper = "<div class='p-1 ml-auto ' >$filtered_wrapper</div>";
        }

        // Total Players found
        $count_data = count($players);
        if (!$count_data){
            $table_row = '';
            $tfp_label = "No data has found";
        }else{
            $tfp_label = $count_data >1 ? "$count_data Players" : "$count_data Player";
        }

        return array(
            $team_code,
            $team, // Source Data
            $players,
            $table_header, // Table for players
            $table_row,    // 
            $filtered_wrapper, // Selected Filter Paramaters ex. /teams?pos=SG&nationality=US
            $tfp_label
        );
    }
    
}