<?php
namespace App\Controllers;
require $_SERVER['DOCUMENT_ROOT'].'/vendor/autoload.php';
use Illuminate\Support;

class Controller  {

    function __constructor(){   
    }

    public function minutesToHours($minutes){
        if ($minutes < 1) {
            return 0;
        }
        $hours = floor($minutes / 60);
        $min = ($minutes % 60);
        return sprintf('%02d:%02d', $hours, $min);
    }

    public function formatDate($date_string, $format){
        if (empty($date_string)) {
            return $date_string;
        }
        try {
            return date_create($date_string)->format($format);
        } catch (\Throwable $th) {
            return 'Invalid Date';
        }
    }

    public function properCase($string){
        if(empty(trim($string))) return $string;
        return mb_convert_case($string, MB_CASE_TITLE, 'utf-8');
    }

    public function upperCase($string){
        if(empty(trim($string))) return $string;
        return mb_convert_case($string, MB_CASE_UPPER, 'utf-8');
    }

    public function lowerCase($string){
        if(empty(trim($string))) return $string;
        return mb_convert_case($string, MB_CASE_LOWER, 'utf-8');
    }

    public function replaceAbbre($string){
        $abbre = ['pct'=>'&#37;','3pt'=>'3 Points', '2pt'=>'2 Points'];
        foreach($abbre as $abb=>$equivalent){
            $string = str_replace($abb, $equivalent, $string);
        }
        return $string;
    }
    
    public function getHeader($data, $exclude_fields = []){
        if(!count($data)){
            return false;
        }
        //Exclude fields
        if($exclude_fields){
            $data = collect($data)->map(function($item, $key) use ($exclude_fields){
                foreach($exclude_fields as $x){
                    unset($item[$x]);
                }
                return $item;
            });
        }
        //Extract field name
        $headings = collect( $data->get( $data->take(1)->keys()[0]) )->keys();
        $headings = $headings->map(function($item, $key) {
            return collect(explode('_', $item))
                ->map(function($item, $key) {
                    return ucfirst($item);
                })
                ->join(' ');
        });
        return $headings;
    }

    function getStaticRowValue($data) {
        if (!count($data)) {
            return 'Sorry, no matching data was found';
        }
        // output data
        $rows = [];
        foreach ($data as $dataRow) {
            $row = '<tr>';
            foreach ($dataRow as $key => $value) {
                $row .= '<td>' . $value . '</td>';
            }
            $row .= '</tr>';
            $rows[] = $row;
        }
        $rows = implode('', $rows);
        return $rows;
    }
 
    /**
     * Debug method - dumps a print_r of any passed variables and exits
     * @param mixed any number of variables you wish to inspect
     */
    function e() {
        $args = func_get_args();
        global $noexit;
        foreach ($args as $arg) {
            $out = print_r($arg, 1);
            echo '<pre>' . $out . '</pre><hr />';
        }
        if (!$noexit) {
            $bt = debug_backtrace();
            exit('<i>Called from: ' . $bt[0]['file'] . ' (' . ($bt[1]['class'] ? $bt[1]['class'] . ':' : '') . $bt[1]['function'] . ')</i>');
        }
    }

    /**
     * Fail method - used in testing to output a decent failure message
     * @param string message the message to output
     */
    function fail($message) {
        exit('<div style="display: inline-block; color: #a94442; background: #f2dede; border: solid 1px #ebccd1; font-family: Helvetica, Arial; size: 16px; padding:8px;"><strong>Error</strong></br><span>'.$message.'</span></div>');
    }

    /**
     * Pass method - used in testing to output a decent pass message
     * @param string message the message to output
     */
    function pass($message) {
        exit('<div style="display: inline-block; color: #3c763d; background: #dff0d8; border: solid 1px #d6e9c6; font-family: Helvetica, Arial; size: 16px; padding: 15px;"><strong>Passed</strong></br><span>'.$message.'</span></div>');
    }

    public function htmlTemplate($html) {
        return <<<HTML
        <html>
            <head>
            <style type="text/css">
                body {
                    font: 12px Roboto, Arial, Helvetica, Sans-serif;
                }
                td, th {
                    padding: 4px 8px;
                    font-size: 12px;
                }
                th {
                    background: #eee;
                    font-weight: 500;
                }
                tr:nth-child(odd) {
                    background: #f4f4f4;
                }
            </style>
            </head>
            <body>
                $html
            </body>
        </html>
        HTML;
    }
}  


 