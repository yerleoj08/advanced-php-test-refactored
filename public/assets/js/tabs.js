(function(){

    const tabs = $('ul.tabs li a');
 
    tabs.on('click', function(e){
        e.preventDefault();
        tabs.removeClass('active');
        $(this).addClass('active');
        resetPane();
        const target = e.target.getAttribute('href');
        const pane = document.querySelector(target);
        pane.classList.add('active');
    })

    const resetPane = () => {
        const tabPanel = document.querySelector('.tab-panel');
        for(let x=0; x <=tabPanel.children.length - 1; x++){
            tabPanel.children[x].classList.remove('active');
        }
    }
})();
