(function(){
    
    var currentUrl = document.URL;

    $('.add_to_filter').on('click', function(){
        var addParams = "&"+getParams($(this));
        if( currentUrl.toString().indexOf($(this).attr('data-key')) ===-1 ){
            let newUrl = currentUrl.toString()+addParams;
            window.location.href = newUrl;
        }
    });

    $('.remove_from_filter').on('click', function(){
        var removeParams = getParams($(this));
        var params = currentUrl.split("?")[1];
        if( typeof params !=='undefined'){
            params = params.split('&');
            if (params.indexOf(removeParams) > 0){
                removeParams ="&"+removeParams;
            }
            if (params.indexOf(removeParams)=== 0 && params.lenght > 1){
                removeParams = removeParams+"&";
            }
        }
        let newUrl = currentUrl.toString().replace(removeParams,'');
        window.location.href = newUrl;
    });

    function getParams(_this){
        var key = _this.attr('data-key');
        var value = _this.attr('data-value');
        return key+"="+value;
    }

    // Export Function
    if($('#form_export').length){
        var form_export = document.getElementById('form_export');
        form_export.addEventListener('submit', function(e){
            e.preventDefault();

            var paramsFilter = getParamsFilter();
            // Parameters
            // var team_code = "team_code="+$('input[name="team_code"]').val();
            var with_stats = $('input[name="ws"]').prop('checked') === true? "ws=1" : '';
            var format = "&fmt="+$('input[name="fmt"]:checked').val();

            var action = e.target.action;
            action += "?" + with_stats + format + paramsFilter;

            $('#form_export input').each(function(){
                var key = $(this).attr('name');
                var value = $(this).val();
                if($(this).attr('type')==='hidden'){
                    console.log(value + " " + key);
                    action+= "&"+key+"="+value;
                }
            });
            console.log(action);
            window.location.href = action;
            
        });
    }
    function getParamsFilter(){
        var params = currentUrl.toString().split("?")[1];;
        if(typeof params !=='undefined'){
            return "&"+params;
        }
        return '';
    }

    //Select Filtering

    $('#team_name').change((e)=>{
        let this_val = e.target.value
        let player_option = document.getElementById('player_name');
        let btn_go = document.getElementById('btn_search_go');
        if(this_val !== ''){
            player_option.removeAttribute('disabled');
            btn_go.removeAttribute('disabled');
        } 
        const $team_code = e.target.value;
        getRequest($team_code);
    });

    if($('#form_find').length){
        var form_find = document.querySelector('#form_find');
        form_find.addEventListener('submit', function(e){
            var find_name = document.querySelector('#player_name');
            if(find_name.value ==='- Select Player -' || find_name.value ===''){
                e.preventDefault();
                alert('Please select Player Name first');
            }
        });
    }
  
    const getRequest = async (team_code) =>{
        document.getElementById('player_name').innerHTML = "<option>- Select Player -</option>";
        let data = await $.ajax({
            url: `/getPlayersByTeamCode?team_code=` + team_code,
            type: 'GET',
            headers:{ 'Content-Type': 'application/json' }
        });
        if(data){
            data = JSON.parse(data);
            data.forEach((v, i)=>{
                optionAppend(v.name, v.id);
            });
        }
    }

    const optionAppend = (value, id) =>{
        const player_option = document.getElementById('player_name');
        let option = document.createElement("option");
        option.text = value;
        option.value = id;
        player_option.appendChild(option);
    }

    $('#more_link').on('click', function(e){
        const mode = $(this).find('small').html();
        if( mode ==='More...'){
            $(this).find('small').html('Less');
            $('.more-info').removeClass('d-none');
            return false;
        }
        $(this).find('small').html('More...');
        $('.more-info').addClass('d-none');
    });

    // $.arrangeColumn = function (table, from, to) {
    //     var rows = $('tr', table);
    //     var cols;
    //     rows.each(function() {
    //         cols = $(this).children('th, td');
    //         cols.eq(from).detach().insertBefore(cols.eq(to));
    //     });
    // }
    // var tbl = $('#players');
    // $.arrangeColumn(tbl, 1, 0);

})();