<?php

global $config;

$config = [
    'default'=>'db_server',
    'db_server' =>[
        'DB_HOST'=>'127.0.0.1',
        'DB_USER'=>'root',
        'DB_PASS'=>'',
        'DB_NAME'=>'nba2019'
    ]
];

return $config;
