<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>NBA 2019 | Players</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link href="/public/assets/css/custom-style.css?version=<?php echo time(); ?>" rel="stylesheet" >
</head>
<body>
    <div class="main" >
        <section class="head">
            <ul class="d-none d-sm-none d-md-block">
                <li><a href="#">Sports Reference</a></li>
                <li><a href="#">Baseball</a></li>
                <li><a href="#">Football(College)</a></li>
                <li><a href="#" class="active">Basketball(College)</a></li>
                <li><a href="#">Hockey</a></li>
            </ul>
            <ul class="ml-auto">
                <li><a href="#">Login</a></li>
                <li><a href="#">Question or Comments?</a></li>
            </ul>
        </section>
        <section class='main-header'>
            <div class="logo col-sm-4">
                <a href="/"><img src="https://d2p3bygnnzw9w3.cloudfront.net/req/202010091/logos/bbr-logo.svg" ></a>
            </div>
            <ul class="main-menu">
                <li><a href="/players" class="active">Players</a></li>
                <li><a href="/teams" class="">Teams</a></li>
                <li><a href="#" >Seasons</a></li>
                <li><a href="#">Scores</a></li>
                <li><a href="#">Playoffs</a></li>
                <li><a href="#">Draft</a></li>
            </ul>
        </section>
        <section class='body'>
            <?php 
                // Preview each Player
                if(count($player)){
                    $ros = $player;
                    $number = $ros['number'];
                    $pos = $ros['pos'];
                    $na = $cont->upperCase($ros['nationality']);
                    $dob = $ros['date_of_birth'];
                    $exp = (is_numeric($ros['years_exp']) ? $ros['years_exp'] : 'N/A') . ((int)($ros['years_exp']) > 1 ? ' Years' : ((int)($ros['years_exp']) ===1 ? ' Year' : ''));
                    $height = $ros['height'];
                    $weight = $ros['weight'];

                    $min_played = $ros['minutes_played'];
                    $_3pt = $ros['3pt'];
                    $_3pct = floor($ros['3pt_pct']);
                    $_2pt = $ros['2pt'];
                    $_2pct = floor($ros['2pt_pct']);
                    $free_throws = $ros['free_throws'];
                    $field_goals = $ros['field_goals'];
                    $assists = $ros['assists'];
                    $steals = $ros['steals'];
                    $total_poinst = number_format($ros['total_points']);
                    
                    echo <<<HTML
                        <div class="header_section">
                            <div class="row">
                                <div class="profile">
                                    <img src="/public/assets/images/players/static_photo.jpg" />
                                </div>
                                <div class="info">
                                    <h1>{$ros['name']}</h1>
                                    <p><strong>Team: </strong> <a href="/teams/preview?team_code={$ros['team_code']}" >{$ros['team']}</a></p>
                                    <p><strong>Number: </strong>{$number}</p>
                                    <p><strong>Position: </strong><a href='/players?pos={$pos}' >{$pos}</a></p>
                                    <p><strong>Born: </strong>{$dob}<strong class="ml-2">Age:</strong> {$ros['age']}</p>
                                    <p><strong>Nationality: </strong><a href='/players?nationality={$na}' >{$cont->upperCase($na)}</a></p>
                                    <p><strong>College: </strong> <a href="http://www.google.com/search?q={$ros['college']}" target="_blank" >{$ros['college']}</a></p>
                                    <p><strong>Experience: </strong>{$exp}</p>
                                    <div>
                                        <div class="more-info d-none">
                                            <p><strong>Height: </strong>{$height}</p>
                                            <p><strong>Weight: </strong>{$weight}</p>
                                        </div>
                                        <a href="javascript:void(0)" id="more_link"><small>More...</small></a>
                                    </div>
                                </div>
                                <div class="pt-3 ml-md-5">
                                    <table class="table bg-dark text-white">
                                        <tbody>
                                            <!-- <tr><td colspan="2"><h1 class="d-sm-none d-md-block">{$ros['overall_3pt_pct']}<small>&#37;<small></h1></td></tr> -->
                                            <tr><td>3 Points ($_3pct&#37;)</td><td>{$_3pt}</td></tr>
                                            <tr><td>2 Points ($_2pct&#37;)</td><td>{$_2pt}</td></tr>
                                            <tr><td>Hr/Min Played</td><td>{$cont->minutesToHours($min_played)}</td></tr>
                                            <tr><td>Field Goals</td><td>{$field_goals}</td></tr>
                                            <tr><td>Assits</td><td>{$assists}</td></tr>
                                            <tr><td>Steal</td><td>{$steals}</td></tr>
                                            <tr><td>TOTAL POINTS</td><td>{$total_poinst}</td></tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="content_section" >
                            <ul class="tabs">
                                <li><a href="#stats" class="active">Stats</a></li>
                                <li><a href="#game-logs" >Game Logs</a></li>
                                <li><a href="#line-up" >Line Up</a></li>
                                <li><a href="#more" >More </a></li>
                                <li><a href="#export" >Export</a></li>
                            </ul>
                            <div class="tab-panel" id="tab-panel" >
                                <div class="tab-pane active" id="stats">
                                    <div class="summary"> 
                                        $stats
                                    </div>
                                </div>
                                <div class="tab-pane" id="game-logs">Game Logs</div>
                                <div class="tab-pane" id="line-up">Line Up</div>
                                <div class="tab-pane" id="more">More</div>
                                <div class="tab-pane" id="export">
                                    <div class="center-center">
                                        <form action="/exports" Method="GET" >
                                            <input type="hidden" name="rpt" value="1">
                                            <label class="mr-4">CSV <input type="radio" name="fmt" value="csv"></label>
                                            <label class="mr-4">JSON <input type="radio" name="fmt" value="json" ></label>
                                            <label class="mr-4">XML <input type="radio" name="fmt" value="xml"></label>
                                            <label class="mr-4">HTML <input type="radio" name="fmt" value="html" checked ></label><br><br>
                                            <input type="hidden" name="id" value="{$ros['id']}">
                                            <input type="hidden" name="filename" value="{$ros['name']}">
                                            <label class="mr-4"><input type="checkbox" name="ws" > Stats</label>
                                            <button type="submit" class="btn-app btn-app-primary" >Export</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        HTML; 
                } else {
                    echo 'No data found';
                }
            ?>
        </section>
        <footer>
           
        </footer>
    </div>

    <script src="/public/assets/js/jquery-1.11.3.min.js"></script>
    <script src="/public/assets/js/app.js"></script>
    <script src="/public/assets/js/tabs.js"></script>
    <script>
    </script>
</body>
</html>