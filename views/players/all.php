<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>NBA 2019 | Players</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link href="/public/assets/css/custom-style.css?version=<?php echo time(); ?>" rel="stylesheet" >
</head>
<body>
    <div class="main" >
        <section class="head">
            <ul class="d-none d-sm-none d-md-block">
                <li><a href="#">Sports Reference</a></li>
                <li><a href="#">Baseball</a></li>
                <li><a href="#">Football(College)</a></li>
                <li><a href="#" class="active">Basketball(College)</a></li>
                <li><a href="#">Hockey</a></li>
            </ul>
            <ul class="ml-auto">
                <li><a href="#">Login</a></li>
                <li><a href="#">Question or Comments?</a></li>
            </ul>
        </section>
        <section class='main-header'>
            <div class="logo col-sm-4">
                <a href="/"><img src="https://d2p3bygnnzw9w3.cloudfront.net/req/202010091/logos/bbr-logo.svg" ></a>
            </div>
            <ul class="main-menu">
                <li><a href="/players" class="active">Players</a></li>
                <li><a href="/teams" class="">Teams</a></li>
                <li><a href="#" >Seasons</a></li>
                <li><a href="#">Scores</a></li>
                <li><a href="#">Playoffs</a></li>
                <li><a href="#">Draft</a></li>
            </ul>
        </section>
        <section class='body'>
            <?php 
                // If Hit letter to filter or other paramter search
                if($player_filtered){
                    $title_label = $filter_by_letter ===null ? "Directory" : "with Last Names Starting with <font class='text-dark-red'>$filter_by_letter</font>";
                    echo <<<HTML
                        <div class="ml-2">
                            <h2>NBA Players $title_label</h2>
                            <h4 class="text-secondary mt-4">Index of Letters</h4>
                            <div class="row pl-2">
                                $letters_options
                            </div>
                            <div class="row mt-4 ml-1">
                                <!-- Total Players found -->
                                <h4 class="text-dark-red my-2">
                                    $tfp_label 
                                </h4>
                                <div class="p-2">
                                    $filtered_wrapper
                                </div>
                                
                                <!-- Table -->
                                <div class="table-responsive">
                                    <table class="table" id="players">
                                        <thead>{$table_header}</thead>
                                        <tbody>{$table_row}</tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="my-30 text-left">
                                <form action="/exports" method="GET" id="form_export" >
                                    <input type="hidden" name="rpt" value="true">
                                    <label class="mr-4">CSV <input type="radio" name="fmt" value="csv"></label>
                                    <label class="mr-4">JSON <input type="radio" name="fmt" value="json" ></label>
                                    <label class="mr-4">XML <input type="radio" name="fmt" value="xml"></label>
                                    <label class="mr-4">HTML <input type="radio" name="fmt" value="html" checked></label><br/><br/>
                                    <label class="mr-4"><input type="checkbox" name="ws" > Stats</label>
                                    <button type="submit" class="btn-app btn-app-primary" $setDisable >Export</button>
                                </form>
                            </div>
                        </div>
                        HTML;
                } else {
                    echo <<<HTML
                        <div class="ml-2">
                            <h2>NBA Player Directory</h2>
                            <h4 class="text-secondary mt-4">Index of Letters</h4>
                            <div class="row pl-2">
                                $letters_options
                            </div>
                            <div class="row">
                                <div class="alpha_list">
                                    $alpha_list
                                </div>
                            </div>
                        </div>
                        HTML;
                }

            ?>
        </section>
        <footer>
           
        </footer>
    </div>

    <script src="/public/assets/js/jquery-1.11.3.min.js"></script>
    <script src="/public/assets/js/app.js"></script>
    <script src="/public/assets/js/tabs.js"></script>

</body>
</html>