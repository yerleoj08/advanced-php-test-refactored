<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>NBA 2019 | Teams</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <link href="/public/assets/css/custom-style.css?version=<?php echo time(); ?>" rel="stylesheet" >
    </head>
    <body>
        <div class="main" >
            <section class="head">
                <ul class="d-none d-sm-none d-md-block">
                    <li><a href="#">Sports Reference</a></li>
                    <li><a href="#">Baseball</a></li>
                    <li><a href="#">Football(College)</a></li>
                    <li><a href="#" class="active">Basketball(College)</a></li>
                    <li><a href="#">Hockey</a></li>
                </ul>
                <ul class="ml-auto">
                    <li><a href="#">Login</a></li>
                    <li><a href="#">Question or Comments?</a></li>
                </ul>
            </section>
            <section class='main-header'>
                <div class="logo col-sm-4">
                <a href="/"><img src="https://d2p3bygnnzw9w3.cloudfront.net/req/202010091/logos/bbr-logo.svg" ></a>
                </div>
                <ul class="main-menu">
                    <li><a href="/players" class="">Players</a></li>
                    <li><a href="/teams" class="active">Teams</a></li>
                    <li><a href="#" >Seasons</a></li>
                    <li><a href="#">Scores</a></li>
                    <li><a href="#">Playoffs</a></li>
                    <li><a href="#">Draft</a></li>
                </ul>
            </section>
            <section class='body'>
                <?php 
                    echo '<div class="row">';
                    foreach($team as $child){
                        echo <<<HTML
                            <div class="team col-12 col-sm-4 col-md-2">
                                <a href="/teams/preview?team_code={$child['code']}" >
                                    <img src="/public/assets/images/logo/{$child['code']}.png" width="150" />
                                </a>
                            </div><br>
                            HTML;
                    }  
                    echo '</div>';
                                     
                    if( !count($team)){
                        echo 'Sorry, No data found';
                    }
                ?>
            </section>
            <footer>
                
            </footer>
        </div>
        <script src="/public/assets/js/jquery-1.11.3.min.js"></script>
        <script src="/public/assets/js/tabs.js"></script>
    </body>
</html>