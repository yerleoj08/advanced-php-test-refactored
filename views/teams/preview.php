<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>NBA 2019 | Teams</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <link href="/public/assets/css/custom-style.css?version=<?php echo time(); ?>" rel="stylesheet" >
    </head>
    <body>
        <div class="main" >
            <section class="head">
                <ul class="d-none d-sm-none d-md-block">
                    <li><a href="#">Sports Reference</a></li>
                    <li><a href="#">Baseball</a></li>
                    <li><a href="#">Football(College)</a></li>
                    <li><a href="#" class="active">Basketball(College)</a></li>
                    <li><a href="#">Hockey</a></li>
                </ul>
                <ul class="ml-auto">
                    <li><a href="#">Login</a></li>
                    <li><a href="#">Question or Comments?</a></li>
                </ul>
            </section>
            <section class='main-header'>
                <div class="logo col-sm-4">
                <a href="/"><img src="https://d2p3bygnnzw9w3.cloudfront.net/req/202010091/logos/bbr-logo.svg" ></a>
                </div>
                <ul class="main-menu">
                    <li><a href="/players" class="">Players</a></li>
                    <li><a href="/teams" class="active">Teams</a></li>
                    <li><a href="#" >Seasons</a></li>
                    <li><a href="#">Scores</a></li>
                    <li><a href="#">Playoffs</a></li>
                    <li><a href="#">Draft</a></li>
                </ul>
            </section>
            <section class='body'>
                <?php 
                    if(count($team)){
                        $s_team = $team[0];
                        echo <<<HTML
                            <div class="header_section">
                                <div class="row">
                                    <div class="logo">
                                        <img src="/public/assets/images/logo/{$s_team['code']}.png" />
                                    </div>
                                    <div class="info">
                                        <h1>{$s_team['name']}</h1>
                                        <p>Location: </p>
                                        <p>Seasons: </p>
                                        <p>Record: </p>
                                        <p>Playoff Appearances: </p>
                                        <p>Championship: </p>
                                    </div>
                                </div>
                            </div>
                            <div class="content_section" >
                                <ul class="tabs">
                                    <li><a href="#players" class="active">Players</a></li>
                                    <li><a href="#game-logs" >Game Logs</a></li>
                                    <li><a href="#export" >Export</a></li>
                                </ul>
                                <div class="tab-panel" id="tab-panel" >
                                    <div class="tab-pane active" id="players">
                                        <div class="row">
                                            <h4 class="text-dark-red mb-2">
                                                $tfp_label 
                                            </h4>
                                            <div class="ml-2">
                                                $filtered_wrapper
                                            </div>
                                        </div>
                                        <div class="table-responsive">
                                            <table class="table">
                                                <thead>{$table_header}</thead>
                                                <tbody>{$table_row}</tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="game-logs">Game Logs</div>
                                    <div class="tab-pane" id="export">
                                        <div class="center-center">
                                            <form action="/exports" method="GET" id="form_export" >
                                                <input type="hidden" name="rpt" value="true">
                                                <label class="mr-4">CSV <input type="radio" name="fmt" value="csv"></label>
                                                <label class="mr-4">JSON <input type="radio" name="fmt" value="json" ></label>
                                                <label class="mr-4">XML <input type="radio" name="fmt" value="xml"></label>
                                                <label class="mr-4">HTML <input type="radio" name="fmt" value="html" checked></label><br><br>
                                                <input type="hidden" name="team_code" value="{$team_code}">
                                                <input type="hidden" name="filename" value="{$team_code}">  
                                                <label class="mr-4"><input type="checkbox" name="ws" > Stats</label>
                                                <button type="submit" class="btn-app btn-app-primary" >Export</button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            HTML; 
                    } else {
                        echo "<h4>No data has found</h4>";
                    }
                ?>
            </section>
            <footer>
                
            </footer>
        </div>
        <script src="/public/assets/js/jquery-1.11.3.min.js"></script>
        <script src="/public/assets/js/app.js"></script>
        <script src="/public/assets/js/tabs.js"></script>

        <script>
            $(document).ready(function(){
 
            });
        </script>
    </body>
</html>