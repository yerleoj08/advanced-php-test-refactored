<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>NBA 2019</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link href="/public/assets/css/custom-style.css?version=<?php echo time(); ?>" rel="stylesheet" >
</head>
<body>
    <div class="main" >
        <section class="head">
            <ul class="d-none d-sm-none d-md-block">
                <li><a href="#">Sports Reference</a></li>
                <li><a href="#">Baseball</a></li>
                <li><a href="#">Football(College)</a></li>
                <li><a href="#" class="active">Basketball(College)</a></li>
                <li><a href="#">Hockey</a></li>
            </ul>
            <ul class="ml-auto">
                <li><a href="#">Login</a></li>
                <li><a href="#">Question or Comments?</a></li>
            </ul>
        </section>
        <section class='main-header'>
            <div class="logo col-sm-4">
                <a href="/"><img src="https://d2p3bygnnzw9w3.cloudfront.net/req/202010091/logos/bbr-logo.svg" ></a>
            </div>
            <ul class="main-menu">
                <li><a href="/players" class="">Players</a></li>
                <li><a href="/teams" class="">Teams</a></li>
                <li><a href="#" >Seasons</a></li>
                <li><a href="#">Scores</a></li>
                <li><a href="#">Playoffs</a></li>
                <li><a href="#">Draft</a></li>
            </ul>
        </section>
        <section class='body'>
            <strong class="mr-2">Basketball Stats and History</strong><span class="fs--1">Statistics, scores, and history for the NBA competition.</span>
            <div class="content_section">
                <div class="row">
                    <div class="col-md-6 p-0">
                        <div class="box">
                            <h4 class="box-title">NBA Players <a href='/players' class="float-right fs--1 text-white p-1">See All</a></h4>
                            <div class="box-body">
                                <div class="row">
                                    <?php echo $top_player; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 p-0 mt-3 mt-md-0">
                        <div class="row">
                            <div class="col-md-6 p-0">
                                <div class="box ml-md-3">
                                    <h4 class="box-title">NBA Teams</h4>
                                    <div class="box-body">
                                        <div class="table-responsive">
                                            <table class="table w-100">
                                                <thead>
                                                    <tr>
                                                        <th>Team</th>
                                                        <th>W</th>
                                                        <th>L</th>
                                                    </tr>
                                                </thead>
                                                <tbody><?php echo $teams; ?></tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="middle" > 
            <h3 class="my-2 text-center border-bottom pb-2">Best Three Pointers</h3>
            <div class="row py-20">
                <div class="col-md-6">
                    <!-- <h3 class="my-2 ">Team</h3> -->
                    <div class="row">
                        <div class="col-md-5 p-0 mt-2">
                            <?php echo $best_3pt_team; ?>
                        </div>
                        <div class="col-md-7">
                            <div class="box">
                                <h4 class="box-title bg-dark">Top <?php echo count($best_3pt_team_players); ?> Players </h4>
                                <div class="box-body">
                                    <div class="table-responsive">
                                        <table class="table w-100">
                                            <thead>
                                                <tr>
                                                    <th>Player</th>
                                                    <th>(Hr/Mn) Played</th>
                                                    <th>3 Pts(&#37;)</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php 
                                                    foreach($best_3pt_team_players as $child){
                                                        echo "<tr>";
                                                        echo "<td><a href='/players/stats?id={$child['id']}'/>{$child['name']}</a></td>";
                                                        echo "<td class='text-center'>{$cont->minutesToHours($child['minutes_played'])}</td>";
                                                        echo "<td>{$child['3pt_pct']}</td>";
                                                        echo "</tr>";
                                                    }
                                                ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="box-footer text-center">
                                    <a href="/teams/preview?team_code=<?php echo $team_code; ?>" class='fs--1'>See all</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6" >
                    <?php 
                            // I used looping just incase we want to set best of 2 or 3 and so on
                            foreach($best_3pt_player as $child){
                                
                                $number = $child['number'];
                                $pos = $child['pos'];
                                $na = $cont->upperCase($child['nationality']);
                                $dob = $child['date_of_birth'];
                                $exp = (is_numeric($child['years_exp']) ? $child['years_exp'] : 'N/A') . ((int)($child['years_exp']) > 1 ? ' Years' : ((int)($child['years_exp']) ===1 ? ' Year' : ''));
                                $height = $child['height'];
                                $weight = $child['weight'];

                                $min_played = $child['minutes_played'];
                                $_3pt = $child['3pt'];
                                $_3pct = floor($child['3pt_pct']);
                                $_2pt = $child['2pt'];
                                $_2pct = floor($child['2pt_pct']);
                                $free_throws = $child['free_throws'];
                                $field_goals = $child['field_goals'];
                                $assists = $child['assists'];
                                $steals = $child['steals'];
                                $total_poinst = number_format($child['total_points']);

                                echo <<<HTML
                                    <div class="" >
                                        <div class="row">
                                            <div class="profile col-lg-3 mt-4 mt-md-0">
                                                <a href="/players/stats?id={$child['id']}"><img src="/public/assets/images/players/static_photo.jpg" /></a>
                                            </div>
                                            <div class="d-flex col-lg-9 pl-lg-4 p-0">
                                                <div class="info col-6 ">
                                                    <h3>{$child['name']}</h3>
                                                    <p><strong>Team: </strong> <a href="/teams/preview?team_code={$child['team_code']}" >{$child['team']}</a></p>
                                                    <p><strong>Number: </strong>{$number}</p>
                                                    <p><strong>Position: </strong><a href='/players?pos={$pos}' >{$pos}</a></p>
                                                    
                                                    <p><strong>Nationality: </strong><a href='/players?nationality={$na}' >{$cont->upperCase($na)}</a></p>
                                                    <p><strong>College: </strong> <a href="http://www.google.com/search?q={$child['college']}" target="_blank" >{$child['college']}</a></p>
                                                    <p><strong>Experience: </strong>{$exp}</p>
                                                </div>
                                                <div class="ml-auto col-6 p-0">
                                                    <table class="table bg-dark text-white ml-4" style="width: 120px !important;">
                                                        <tbody>
                                                            <tr><td colspan="2"><h1 class="d-sm-none d-md-block">{$child['overall_3pt_pct']}<small>&#37;<small></h1></td></tr>
                                                            <tr><td>3 Points ($_3pct&#37;)</td><td>{$_3pt}</td></tr>
                                                            <tr><td>2 Points ($_2pct&#37;)</td><td>{$_2pt}</td></tr>
                                                            <tr><td>Hr/Min Played</td><td>{$cont->minutesToHours($min_played)}</td></tr>
                                                            <tr><td>Field Goals</td><td>{$field_goals}</td></tr>
                                                            <tr><td>Assits</td><td>{$assists}</td></tr>
                                                            <tr><td>Steal</td><td>{$steals}</td></tr>
                                                            <tr><td>TOTAL POINTS</td><td>{$total_poinst}</td></tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            
                                        </div>
                                    </div>
                                    HTML;
                            }
                       
                        ?>
                </div>
            </div>
        </section>
        <section class="search" >
            <div class="center-center d-flex flex-column" style="width:100%; height: 100%;">
                <h2 class="text-center text-white">Find Active Player</h2>
                <div class="py-20">
                    <form action="/players/stats" method="GET" id="form_find">
                        <div class="form-group d-flex flex-column">
                            <select id="team_name" >
                                <?php echo $teams_options; ?>
                            </select>
                            <select name="id" id="player_name" disabled class="mt-2">
                                <option value="">- Select Player -</option>
                            </select>
                        </div>
                        <div class="col-12 text-center">
                            <button class="btn-app btn-lg btn-app-primary" id="btn_search_go" disabled>Go</button>
                        </div>
                    </form>
                </div>
            </div>
        </section>
        <section class="news"></section>
        <footer></footer>
    </div>

    <script src="/public/assets/js/jquery-1.11.3.min.js"></script>
    <script src="/public/assets/js/app.js"></script>

</body>
</html>