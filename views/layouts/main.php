<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>NBA 2019</title>
    <link href="/public/assets/css/bootstrap-4.min.css" rel="stylesheet" >
    <link href="/public/assets/css/custom-style.css?version=<?php echo time(); ?>" rel="stylesheet" >
</head>
<body>
    <div class="main" ></div>
    <script src="/public/assets/js/jquery-1.11.3.min.js"></script>
    <script src="/public/assets/js/app.js"></script>
</body>
</html>
